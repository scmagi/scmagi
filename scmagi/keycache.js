const CACHE_TIME = 3600000;

const xmpp_jid = require("@xmpp/jid");
const keycache = {};

module.exports = function readwrite(jid, data){
    const jid_index = xmpp_jid(jid).bare().toString();
    const now = new Date().getTime();
    for(let jid in keycache){
        if(now - keycache[jid].lastupdate > CACHE_TIME){
            delete keycache[jid];
        }
    }

    if(undefined !== data){
        keycache[xmpp_jid(jid).bare().toString()] = {
            data: data,
            lastupdate: now,
        }
        return data;
    } else {
        if(undefined == keycache[jid_index]) return null;
        return keycache[jid_index].data;
    }

}

const msgpack = require("@ygoe/msgpack");

module.exports = {

    pack: function(data, format){
        if(!format) format = "base64";
        return Buffer.from(msgpack.serialize(data)).toString(format);
    },

    unpack: function(data, format){
        if(!format) format = "base64";
        return msgpack.deserialize(Buffer.from(data, "base64"));
    },


}

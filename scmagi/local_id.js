const RENEW_STATUS_INTERVAL = 3600000;
const RENEW_BEFORE = 600000;
const ACCEPTED_LIFE_OF_STATUS_MAX = 3600000;

const nacl = require("tweetnacl");
const { pack, unpack } = require("./datapack");
const events = require("events");



class LocalIdentifier extends events.EventEmitter{

    constructor(local_private_key){ 
        super();
        const seed = nacl.hash(local_private_key).slice(0, 32);

        let local_id_keypair = nacl.sign.keyPair.fromSeed(seed);
        this.id_sign = function(message){
            return nacl.sign(message, local_id_keypair.secretKey);
        }
        this.id_sign_detached = function(message){
            return nacl.sign.detached(message, local_id_keypair.secretKey);
        }
        this.id_public_key = local_id_keypair.publicKey;

        this.generate_local_status();
        setInterval(
            ()=>this.generate_local_status(),
            RENEW_STATUS_INTERVAL - RENEW_BEFORE
        );
    }

    generate_local_status(){
        let temp_keypair = nacl.box.keyPair();
        let new_status_unsigned_as_serialized = Buffer.from(pack({
            id_key: this.id_public_key,
            encrypt_key: temp_keypair.publicKey,
            status_id: nacl.randomBytes(32),
            not_before: new Date(),
            not_after: new Date(new Date().getTime() + RENEW_STATUS_INTERVAL),
        }), "ascii");
        let new_status_signature_detached = this.id_sign_detached(
            new_status_unsigned_as_serialized);
        this.status = {
            status: new_status_unsigned_as_serialized,
            signature: new_status_signature_detached,
        };
        this.decrypt = function(box, nonce, remote_public_key){
            return nacl.box.open(
                box,
                nonce,
                remote_public_key, 
                temp_keypair.secretKey
            );
        }
        return this.status; /* { status: <serialized>, signature: <buffer> } */
    }

    verify_remote_status(remote_status_signed){
        let remote_status_serialized = remote_status_signed.status;
        let remote_signature = remote_status_signed.signature;
        let remote_status;
        // deserialize
        try{
            remote_status = unpack(remote_status_serialized.toString());
            // TODO format check in remote_status items
        } catch(e){
            return false;
        }
        // verify self signature
        try{
            if(!nacl.sign.detached.verify(
                new Uint8Array(Buffer.from(remote_status_serialized)),
                remote_signature,
                remote_status.id_key
            )){
                return false;
            }
        } catch(e){
            return false;
        }
        // verify "not before" and "not after" time
        const now = new Date().getTime();
        try{
            if(remote_status.not_before.getTime() > now){
                // not before cannot be in future
                return false;
            }
            let valid_diff = remote_status.not_after.getTime()
                - remote_status.not_before.getTime();
            if(!(0 < valid_diff && valid_diff <= ACCEPTED_LIFE_OF_STATUS_MAX)){
                // not after too far after not before
                return false;
            }
            if(remote_status.not_after.getTime() < now){
                // key expired
                return false;
            }
        } catch(e){
            console.error(e);
            return false;
        }
        return remote_status;
    }



}


module.exports = LocalIdentifier;


/*
let a = new LocalIdentifier(new Uint8Array(32));
let b = new LocalIdentifier(new Uint8Array(64));

console.log(b.verify_remote_status(a.generate_local_status()));
*/

const nacl = require("tweetnacl");
const { pack, unpack } = require("./datapack");
const events = require("events");

const LocalIdentifier = require("./local_id");
const node = require("./node");
const keycache = require("./keycache");




var secure_node = null;

class SCMAGISecureNode extends events.EventEmitter {

    constructor(local_private_key){
        super();

        this.local_id = new LocalIdentifier(local_private_key);
        this.node = node.node();

        // local status updater



    }

    async publish(target_jid, data){
        
    }


    

    



}






function make_secure_node(local_private_key, scmagi_node){
}



module.exports = {
    init: function(options){
        if(secure_node !== null) return secure_node;
        // PUBLIC KEY -> resource
        // options.resource = ...
        const private_key = options.private_key;
        delete options.private_key;
        node.init(options);
        secure_node = new SCMAGISecureNode(private_key);
    }
}


let test = new SCMAGISecureNode(
    new Uint8Array(Buffer.from('secret')),
    null
);

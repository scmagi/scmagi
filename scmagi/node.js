const { client, xml } = require("@xmpp/client");
const { pack, unpack } = require("./datapack");
const events = require("events");

const RPCNS = "http://bitbucket.org/scmagi";

const RET_SUCCESS = xml("scmagi-success", { xmlns: RPCNS }); 
const RET_BADREQUEST = xml("scmagi-error", { xmlns: RPCNS }, "bad-request");



var xmppclient, scmagi_node;

function init(options){
    if(null != xmppclient) return;
    xmppclient = client(options);    
    handle_xmpp_error(xmppclient);
    xmppclient.start();

    scmagi_node = new SCMAGINode();
    return scmagi_node
}


function handle_xmpp_error(client){
    client.on("error", async function(e){
        console.error("XMPP client error:", e);
        console.warn("Shutting down XMPP for reconnect.");
        while(true){
            try{
                await client.stop();
                await client.start();
                break;
            } catch(e){
                await new Promise(
                    (resolve, reject)=>setTimeout(resolve, 10000));
            }
        }
    });
}






class SCMAGINode extends events.EventEmitter {

    constructor (){
        super();
        const self = this;

        this.client = xmppclient;
        this.timeout = 30000;

        this.client.iqCallee.set(RPCNS, "scmagi", async (ctx)=>{
            const jid_from = ctx.from.toString();

            let result = null;
            try{
                result = unpack(ctx.element.text());
            } catch(e){
                return RET_BADREQUEST;
            }

            this.emit("data", { from: jid_from, data: result });
            return RET_SUCCESS;

        });

        this.client.on("online", ()=>{
            this.emit("online");
        });
    }


    async publish(target_jid, data){
        if(!target_jid){
            throw Error("no-receiver");
        }

        let result;
        try{
            result = await this.client.iqCaller.request(
                xml("iq", { type: "set", to: target_jid },
                    xml(
                        "scmagi",
                        { xmlns: RPCNS },
                        pack(data)
                    )
                )
            );
        } catch(e){
            return { error: e.message }
        }

        try{
            if(result.children[0].name == "scmagi-success"){
                return { success: true };
            } else {
                return { error: result.children[0].text() };
            }
        } catch(e){
            return { error: e.message };
        }

    }
}








module.exports = { 
    init: init,
    node: ()=>scmagi_node,
    get_jid: ()=>xmppclient.jid.toString(),
    get_bare_jid: ()=>xmppclient.jid.bare().toString(), 
};




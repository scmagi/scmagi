scmagi: Exchanging data packets over XMPP
=========================================

This library provides a simple interface for sending data packets over XMPP.
It intends to have a similar function to WebSocket.

By replacing WebSocket with XMPP, this library allows communication between
clients, not only client and server. Furthermore, a federated structure
resulted from XMPP will also apply to any application built upon this. The user
may login via another XMPP account and will still be able to send data packets,
making a web application more robust against single point of failure.

## Usage

```js
const { node: scmagi_node, init: scmagi_init } = require("scmagi");
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'; // YOU MAY NOT NEED THIS

const scmagi = scmagi_init({
    service:  "xmpp://localhost",
    username: "server",
    password: "testpassword",
    resource: "scmagi", // currently this is hardcoded
});

scmagi.on("online", async function (){
    console.log("online");

    let data = { info: "hello" };
    console.log(
        "request",
        await scmagi.publish("client@localhost/scmagi", data)
    );
});

scmagi.client.on("stanza", (e)=>console.log(e.toString()));

scmagi.on("data", function({ from, data }){
    console.log("received", data );
});
```

This example code will login automatically to a locally installed XMPP server
using given username and password. Upon going online, it sends a piece of
data to `client@localhost/scmagi`, another account. That's all.
